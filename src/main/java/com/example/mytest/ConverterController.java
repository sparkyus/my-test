package com.example.mytest;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.io.IOException;


//This is the converter controller that, by means of the method convertLbToKg,
// the number that is introduced, as long as it is not negative, when clicking on the convert button,
// will give you the result of changing the unit of weight from Lb to Kg.
public class ConverterController {
    @FXML
    private TextField txtLb;

    @FXML
    private Label lblResult;

    @FXML
    protected void changeToCalculator() throws IOException {
        HelloApplication.setRoot("hello-view");
    }

    @FXML
    protected void convertLbToKg() {
        try {
            double lb = Double.parseDouble(txtLb.getText());
            double kg = lb * 0.45359237;
            lblResult.setText(String.format("%.2f lb are %.2f kg", lb, kg));
        } catch (NumberFormatException e) {
            lblResult.setText("Insert valid value of lb.");
        }
    }
}
