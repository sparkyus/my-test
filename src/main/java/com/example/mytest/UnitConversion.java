package com.example.mytest;

public class UnitConversion {

    static int convertHexadecimal(String number) {
        int converted;
        try {
            converted = Integer.parseInt(number, 16);
        } catch (NumberFormatException e) {
            converted = 0;
        }
        return converted;
    }

    static int convertBinary(String number) {
        int converted;

        try {
            converted = Integer.parseInt(number, 2);
        } catch (NumberFormatException e) {
            converted = 0;
        }
        return converted;
    }
}
