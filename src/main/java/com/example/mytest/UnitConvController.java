package com.example.mytest;

import javafx.fxml.FXML;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;

import javax.swing.*;
import java.io.IOException;

public class UnitConvController {
    public RadioButton radioHexadecimal;
    public RadioButton radioBinary;

    @FXML
    private TextField inputBox;

    @FXML
    protected void changeToCalculator() throws IOException {
        HelloApplication.setRoot("hello-view");
    }

    @FXML
    protected void doConversion() {
        int result = 0;
        if (radioBinary.isSelected()) {
            result = UnitConversion.convertBinary(inputBox.getText());
        } else if (radioHexadecimal.isSelected()) {
            result = UnitConversion.convertHexadecimal(inputBox.getText());
        } else {
            JOptionPane.showMessageDialog(null, "Choose a conversion format.", "Information", JOptionPane.INFORMATION_MESSAGE);
        }
        inputBox.setText(Integer.toString(result));
    }
}
